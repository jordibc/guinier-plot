{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "magnetic-primary",
   "metadata": {},
   "source": [
    "# Guinier Plot"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "neither-conversation",
   "metadata": {},
   "source": [
    "What is a Guinier plot? This notebook will try to define it clearly and show step by step how to create one. Its reference document will be [Rosenthal & Henderson (2003)](https://pubmed.ncbi.nlm.nih.gov/14568533/) and [Vilas et al. (2020)](https://pubmed.ncbi.nlm.nih.gov/31911170/)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "northern-klein",
   "metadata": {},
   "source": [
    "In the future, I'd like to expand it to also show how it is useful in cryo-electron microscopy, providing a tutorial to both create and interpret such plots. Maybe? :)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "active-carpet",
   "metadata": {},
   "outputs": [],
   "source": [
    "%run ../cryoem-common/cryoem_common.ipynb"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "square-support",
   "metadata": {},
   "outputs": [],
   "source": [
    "add_latex_commands()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "golden-blade",
   "metadata": {},
   "source": [
    "A Guinier plot represents the logarithm of the squared amplitudes radially averaged of the Fourier transform of the macromolecule *vs* frequency squared:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "painted-passion",
   "metadata": {},
   "source": [
    "$$\n",
    "\\log(F) \\, \\text{vs.} \\, 1/d^2\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "quality-combining",
   "metadata": {},
   "source": [
    "with $F$ being the spherically averaged structure factor in a resolution shell with frequency $f = 1/d$:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "complex-bridges",
   "metadata": {},
   "source": [
    "$$\n",
    "F(f) = \\int_\\vect{f} S_f(\\vect{f}) \\, | \\F{V}\\!(\\vect{f}) |^2\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "finite-charleston",
   "metadata": {},
   "source": [
    "where $S_f$ is that \"shell with frequency $f$\" (to be defined later), and the squared amplitude of the Fourier transform of the volume $|\\F{V}|^2$ is what we call the **structure factor**."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "scheduled-lighting",
   "metadata": {},
   "source": [
    "## Reading a volume"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "attended-doctrine",
   "metadata": {},
   "outputs": [],
   "source": [
    "V, voxel_n, voxel_size = get_vol_and_voxel('emd_10418_half_map_1.map')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bound-earth",
   "metadata": {},
   "source": [
    "We have a volume with the given dimensions and sample spacing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "expensive-blind",
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Dimensions: %dx%dx%d' % (voxel_n, voxel_n, voxel_n))\n",
    "print('Spacing: %g (A)' % voxel_size)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ignored-restaurant",
   "metadata": {},
   "source": [
    "This is how it looks like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "tired-parcel",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot(V, title='Volume')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "creative-photograph",
   "metadata": {},
   "source": [
    "## Selecting frequencies"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "collected-creativity",
   "metadata": {},
   "source": [
    "To make a radial average in the frequency domain, we define $S_f$, the \"*shell at frequency* $f$\", which is a function such that $S_f(\\vect{f'})$ is a normalized smooth approximation to $\\delta(\\|\\vect{f'}\\| - f)$. For example, we can use $S_f(\\vect{f'}) \\propto e^{- \\frac{(\\|\\vect{f'}\\| - f)^2}{2 \\sigma^2}}$ for a certain $\\sigma$, that we can take to cover several pixels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "duplicate-induction",
   "metadata": {},
   "outputs": [],
   "source": [
    "def shell(f):\n",
    "    \"Return a normalized shell of spatial frequencies, around frequency f\"\n",
    "    S = exp(- (f_norm - f)**2 / (2 * f_width**2))\n",
    "    return S / sum(S)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "humanitarian-austin",
   "metadata": {},
   "outputs": [],
   "source": [
    "f_voxel_width = 2  # width (in voxels) of the shell\n",
    "f_width = f_voxel_width / (voxel_n * voxel_size)  # frequency width"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "hollow-campaign",
   "metadata": {},
   "source": [
    "We define a volume in frequency space that at each frequency voxel $\\vect{f}$ contains the norm $f = \\norm{\\vect{f}}$, so we can reuse it in each new call to `shell(f)`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "found-settlement",
   "metadata": {},
   "outputs": [],
   "source": [
    "fx, fy, fz = fftnfreq(voxel_n, d=voxel_size)\n",
    "f_norm = sqrt(fx**2 + fy**2 + fz**2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "electric-cambodia",
   "metadata": {},
   "source": [
    "With those values, we can see how the `shell` function will look like in frequency space."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "raised-chemical",
   "metadata": {},
   "outputs": [],
   "source": [
    "fplot(shell(f=0.2), title='Frequency shell at $f = 0.2 \\\\, \\\\AA^{-1} \\\\, (d = 5 \\\\, \\\\AA)$')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "determined-spending",
   "metadata": {},
   "source": [
    "## Guinier Plot"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "abstract-float",
   "metadata": {},
   "source": [
    "We now have all the elements needed to do the Guinier plot:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "three-hobby",
   "metadata": {},
   "outputs": [],
   "source": [
    "F = abs(fftn(V))**2  # structure factor"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "sudden-april",
   "metadata": {},
   "outputs": [],
   "source": [
    "def log_F(f):\n",
    "    S = shell(f)\n",
    "    F_at_f = sum(S * F)  # structure factor averaged at frequency f\n",
    "    return log(F_at_f)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "shaped-gabriel",
   "metadata": {},
   "outputs": [],
   "source": [
    "fmax = 1 / (2 * voxel_size)  # maximum possible frequency (nyquist)\n",
    "f2 = linspace(0, fmax**2, 50)\n",
    "freqs = sqrt(f2)\n",
    "\n",
    "with Pool() as pool:\n",
    "    log_Fs = pool.map(log_F, freqs)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "particular-cabin",
   "metadata": {},
   "source": [
    "And let's look at the results!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "absolute-hello",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(9, 6))\n",
    "plt.plot(f2, log_Fs)\n",
    "plt.title('Guinier Plot')\n",
    "plt.xlabel('$1/d^2 (\\\\AA^{-2})$')\n",
    "plt.ylabel('$\\\\log(F)$');"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "disabled-ensemble",
   "metadata": {},
   "source": [
    "See a peak at $1/d^2 \\approx 0.04$ ? That is around 5 Å, which corresponds to the frequencies of [alpha helices](https://en.wikipedia.org/wiki/Alpha_helix) and [beta sheets](https://en.wikipedia.org/wiki/Beta_sheet)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
